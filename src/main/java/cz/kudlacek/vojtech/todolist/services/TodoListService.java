package cz.kudlacek.vojtech.todolist.services;

import cz.kudlacek.vojtech.todolist.DAOs.TodoItemRepository;
import cz.kudlacek.vojtech.todolist.DAOs.TodoListDAO;
import cz.kudlacek.vojtech.todolist.datamodel.TodoItem;
import cz.kudlacek.vojtech.todolist.datamodel.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

@Service
public class TodoListService {

    private final TodoListDAO todoLists;
    private final TodoItemRepository itemRepository;

    @Autowired
    public TodoListService(@Qualifier("todoListMySQLDAO") TodoListDAO todoLists, TodoItemRepository itemRepository) {
        this.todoLists = todoLists;
        this.itemRepository = itemRepository;
    }

    public List<TodoList> getTodoLists() {
        List<TodoList> data = todoLists.getAllData();
        data.sort(Comparator.comparing(TodoList::getName));
        return data;
    }

    public int create(TodoList todoList) {
        if (todoLists.getDataByName(todoList.getName()) != null)
            return 500;
        this.todoLists.addData(todoList);
        return 200;
    }

    public int addItem(String nameOfTodoList, TodoItem item) {
        TodoList todoList = todoLists.getDataByName(nameOfTodoList);
        if (todoList == null)
            return 500;
        this.itemRepository.save(item);
        todoList.addItem(item);
        todoLists.addData(todoList);
        return 200;
    }

    public TodoList edit(String oldName,
                         String name,
                         Color color) {
        TodoList list = todoLists.getDataByName(oldName);
        System.out.println(list);
        if (list == null)
            return null;
        TodoList todoList = todoLists.getDataByName(name);
        System.out.println(todoList);
        if (todoList != null)
            return null;
        todoLists.deleteData(list);
        list.setName(name);
        list.setColor(color);
        todoLists.addData(list);
        return todoLists.getDataByName(name);
    }

    public int deleteItem(String listName, String nameOfItem) {
        TodoList todoList = todoLists.getDataByName(listName);
        if (todoList == null)
            return 500;
        List<TodoItem> items = todoList.getTodoItems();
        items.removeIf(e -> e.getTitle().equals(nameOfItem));
        todoList.setTodoItems(new HashSet<>(items));
        create(todoList);
        this.itemRepository.deleteById(nameOfItem);
        return 200;
    }

    public int delete(String name) {
        TodoList toDelete = todoLists.getDataByName(name);
        if (toDelete == null)
            return 500;
        List<TodoItem> deleteItems = toDelete.getTodoItems();
        todoLists.deleteData(toDelete);
        deleteItems.forEach(this.itemRepository::delete);
        return 200;
    }
}
