package cz.kudlacek.vojtech.todolist.services;

import cz.kudlacek.vojtech.todolist.DAOs.ImportanceDAO;
import cz.kudlacek.vojtech.todolist.DAOs.ImportanceMySQLDAO;
import cz.kudlacek.vojtech.todolist.datamodel.Importance;
import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;

@Service
public class ImportanceService {

    private final ImportanceDAO importances;

    @Autowired
    public ImportanceService(@Qualifier("importanceMySQLDAO") ImportanceDAO importance) {
        this.importances = importance;
    }

    public List<Importance> getImportances() {
        List<Importance> data = importances.getAllData();
        data.sort(Importance::compareTo);
        return importances.getAllData();
    }

    public int createImportance(int LVL, String name, Color color) {
        List<Importance> data = importances.getAllData();
        if (data.stream()
                .anyMatch(e -> e.getName().equals(name)))
            return 500;
        this.importances.addData(new Importance(LVL, name, color));
        return 200;
    }

    public Importance editImportance(String originalName, int LVL, String name, Color color) {
        List<Importance> data = importances.getAllData();
        Importance toReplace = importances.getDataByName(originalName);
        if (toReplace == null)
            return null;
        int index = data.indexOf(toReplace);
        this.importances.setData(index, new Importance(LVL, name, color));
        return this.importances.getAllData().get(index);
    }

    public int delete(String name) {
        Importance toDelete = importances.getDataByName(name);
        if (toDelete == null)
            return 404;

        importances.deleteData(toDelete);
        return 200;
    }
}
