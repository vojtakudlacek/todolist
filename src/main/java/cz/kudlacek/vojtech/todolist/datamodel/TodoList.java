package cz.kudlacek.vojtech.todolist.datamodel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class TodoList {
    @Id
    private String name;
    @OneToMany
    private Set<TodoItem> todoItems;
    private Color color;

    public TodoList(String name, Set<TodoItem> todoItems, Color color) {
        this.name = name;
        this.todoItems = todoItems;
        this.color = color;
    }

    public TodoList() {
    }

    public String getName() {
        return name;
    }

    public void setTodoItems(Set<TodoItem> todoItems) {
        this.todoItems = todoItems;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TodoItem> getTodoItems() {
        return new ArrayList<>(todoItems);
    }

    public void addItem(TodoItem item) {
        this.todoItems.add(item);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "TodoList{" +
                "name='" + name + '\'' +
                ", todoItems=" + todoItems +
                ", color=" + color +
                '}';
    }
}
