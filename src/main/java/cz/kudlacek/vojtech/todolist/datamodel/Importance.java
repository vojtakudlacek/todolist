package cz.kudlacek.vojtech.todolist.datamodel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.awt.*;

@Entity
@Table(name = "importance")
public class Importance implements Comparable<Importance> {
    @Id
    private int LVL;
    private String name;
    private Color color;

    public Importance(int LVL, String name, Color color) {
        this.LVL = LVL;
        this.name = name;
        this.color = color;
    }

    public Importance() {
        name = "New Importance LVL: " + LVL;
        color = new Color(LVL);
    }

    public int getLVL() {
        return LVL;
    }

    public void setLVL(int LVL) {
        this.LVL = LVL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public int compareTo(Importance o) {
        return Integer.compare(o.LVL, this.LVL);
    }

    @Override
    public String toString() {
        return "Importance{" +
                "LVL=" + LVL +
                ", name='" + name + '\'' +
                ", color=" + color +
                '}';
    }

}
