package cz.kudlacek.vojtech.todolist.datamodel;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "todo_item")
public class TodoItem {
    @Id
    private String title;
    private String description;
    @GeneratedValue
    private LocalDateTime dateTimeOfCreation;
    private LocalDateTime dueDateTime;
    @OneToOne
    private Importance importanceLevel;

    public TodoItem(String title,
                    String description,
                    LocalDateTime dateTimeOfCreation,
                    LocalDateTime dueDateTime,
                    Importance importanceLevel) {
        this.title = title;
        this.description = description;
        this.dateTimeOfCreation = dateTimeOfCreation;
        this.dueDateTime = dueDateTime;
        this.importanceLevel = importanceLevel;
    }

    protected TodoItem() {
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDateTimeOfCreation() {
        return dateTimeOfCreation;
    }

    public LocalDateTime getDueDateTime() {
        return dueDateTime;
    }

    public Importance getImportanceLevel() {
        return importanceLevel;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDueDateTime(LocalDateTime dueDateTime) {
        this.dueDateTime = dueDateTime;
    }

    public void setImportanceLevel(Importance importanceLevel) {
        this.importanceLevel = importanceLevel;
    }

    @Override
    public String toString() {
        return "TodoItem{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dateTimeOfCreation=" + dateTimeOfCreation +
                ", dueDateTime=" + dueDateTime +
                ", importanceLevel=" + importanceLevel +
                '}';
    }
}
