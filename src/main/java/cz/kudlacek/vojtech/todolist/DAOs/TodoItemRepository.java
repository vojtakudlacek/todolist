package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoItemRepository extends JpaRepository<TodoItem, String> {
}
