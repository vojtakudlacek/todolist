package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.TodoList;
import org.springframework.data.repository.CrudRepository;

public interface TodoListDataRepository extends CrudRepository<TodoList, String> {
}
