package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoListMySQLDAO implements TodoListDAO {

    private final TodoListDataRepository todoListDataRepository;

    @Autowired
    public TodoListMySQLDAO(TodoListDataRepository todoListDataRepository) {
        this.todoListDataRepository = todoListDataRepository;
    }

    @Override
    public List<TodoList> getAllData() {
        return (ArrayList<TodoList>) todoListDataRepository.findAll();
    }

    @Override
    public TodoList getDataByName(String name) {
        return todoListDataRepository.findById(name).orElse(null);
    }

    @Override
    public void addData(TodoList todoList) {
        todoListDataRepository.save(todoList);
    }

    @Override
    public void setData(int index, TodoList todoList) {
        deleteData(index);
        addData(todoList);
    }

    @Override
    public void deleteData(int index) {
        List<TodoList> data = getAllData();
        todoListDataRepository.delete(data.get(index));
    }

    @Override
    public void deleteData(TodoList toDelete) {
        todoListDataRepository.delete(toDelete);
    }
}
