package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.Importance;

import java.util.List;

public interface ImportanceDAO {
    List<Importance> getAllData();
    Importance getDataByName(String name);
    void addData(Importance importance);
    void setData(int index, Importance importance);
    void deleteData(int index);
    void deleteData(Importance toDelete);
}
