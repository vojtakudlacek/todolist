package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.TodoList;

import java.util.List;

public interface TodoListDAO {

    List<TodoList> getAllData();
    TodoList getDataByName(String name);
    void addData(TodoList todoList);
    void setData(int index, TodoList todoList);
    void deleteData(int index);
    void deleteData(TodoList toDelete);
}
