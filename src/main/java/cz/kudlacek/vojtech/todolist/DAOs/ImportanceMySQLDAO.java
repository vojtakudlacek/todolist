package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.Importance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImportanceMySQLDAO implements ImportanceDAO {

    private final ImportanceRepository importanceRepository;

    @Autowired
    public ImportanceMySQLDAO(ImportanceRepository importanceRepository) {
        this.importanceRepository = importanceRepository;
    }

    @Override
    public List<Importance> getAllData() {
        return (ArrayList<Importance>) importanceRepository.findAll();
    }

    @Override
    public Importance getDataByName(String name) {
        return getAllData().stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void addData(Importance importance) {
        importanceRepository.save(importance);
    }

    @Override
    public void setData(int index, Importance importance) {
        deleteData(index);
        addData(importance);
    }

    @Override
    public void deleteData(int index) {
        Importance toDelete = getAllData().get(index);
        deleteData(toDelete);
    }

    @Override
    public void deleteData(Importance toDelete) {
        importanceRepository.delete(toDelete);
    }
}
