package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.Importance;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImportanceFakeDAO implements ImportanceDAO {

    private final List<Importance> data = new ArrayList<>();

    public List<Importance> getAllData() {
        return data;
    }

    public Importance getDataByName(String name) {
        return data.stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    public void addData(Importance importance) {
        data.add(importance);
    }

    public void setData(int index, Importance importance) {
        data.set(index, importance);
    }

    public void deleteData(int index) {
        data.remove(index);
    }

    public void deleteData(Importance toDelete) {
        data.remove(toDelete);
    }
}
