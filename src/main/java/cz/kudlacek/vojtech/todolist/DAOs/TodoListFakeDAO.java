package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.TodoList;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoListFakeDAO implements TodoListDAO {

    private final List<TodoList> todoLists = new ArrayList<>();

    public List<TodoList> getAllData() {
        return todoLists;
    }

    public TodoList getDataByName(String name) {
        return todoLists.stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    public void addData(TodoList todoList) {
        todoLists.add(todoList);
    }

    public void setData(int index, TodoList todoList) {
        todoLists.set(index, todoList);
    }

    public void deleteData(int index) {
        todoLists.remove(index);
    }

    public void deleteData(TodoList toDelete) {
        todoLists.remove(toDelete);
    }
}
