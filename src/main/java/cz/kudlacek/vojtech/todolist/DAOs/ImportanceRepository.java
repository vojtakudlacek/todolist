package cz.kudlacek.vojtech.todolist.DAOs;

import cz.kudlacek.vojtech.todolist.datamodel.Importance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImportanceRepository extends JpaRepository<Importance, Integer> {
}
