package cz.kudlacek.vojtech.todolist.controllers;

import cz.kudlacek.vojtech.todolist.datamodel.Importance;
import cz.kudlacek.vojtech.todolist.services.ImportanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;

@RestController
public class ImportanceController {
    private final ImportanceService importance;

    @Autowired
    public ImportanceController(ImportanceService service) {
        this.importance = service;
    }

    @GetMapping()
    @RequestMapping(value = "importance/getAll", produces = ("application/json"))
    public List<Importance> getAll() {
        return importance.getImportances();
    }

    @PostMapping()
    @RequestMapping(value = "importance/create")
    public int create(@RequestParam int lvl,
                      @RequestParam String name,
                      @RequestParam int r, @RequestParam int b, @RequestParam int g) {
        return importance.createImportance(lvl, name, new Color(r, g, b));
    }

    @PutMapping()
    @RequestMapping(value = "importance/edit")
    public Importance edit(@RequestParam String originalName,
                           @RequestParam int lvl,
                           @RequestParam String name,
                           @RequestParam int r, @RequestParam int g, @RequestParam int b) {
        return importance.editImportance(originalName, lvl, name, new Color(r, g, b));
    }

    @DeleteMapping()
    @RequestMapping(value = "importance/delete")
    public int delete(@RequestParam String name){
        return this.importance.delete(name);
    }

}
