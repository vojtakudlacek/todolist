package cz.kudlacek.vojtech.todolist.controllers;

import cz.kudlacek.vojtech.todolist.datamodel.TodoItem;
import cz.kudlacek.vojtech.todolist.datamodel.TodoList;
import cz.kudlacek.vojtech.todolist.services.ImportanceService;
import cz.kudlacek.vojtech.todolist.services.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;

@RestController
public class TodoListController {
    private final TodoListService service;
    private final ImportanceService importanceService;

    @Autowired
    public TodoListController(TodoListService service, ImportanceService importanceService) {
        this.service = service;
        this.importanceService = importanceService;
    }

    @GetMapping
    @RequestMapping(value = "todolist/getAll", produces = ("application/json"))
    public List<TodoList> getAll() {
        return service.getTodoLists();
    }

    @PostMapping
    @RequestMapping(value = "todolist/create", produces = ("application/json"))
    public int create(@RequestParam String name, @RequestParam int r, @RequestParam int b, @RequestParam int g) {
        return service.create(new TodoList(name, new HashSet<>(), new Color(r, g, b)));
    }

    @PostMapping
    @RequestMapping(value = "todolist/addItem", produces = ("application/json"))
    public int addItem(@RequestParam String nameOfTodoList,
                       @RequestParam String titleOfTask,
                       @RequestParam String description,
                       @RequestParam
                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dueDate,
                       @RequestParam int importanceLevel) {
        return service.addItem(nameOfTodoList,
                new TodoItem(titleOfTask, description, LocalDateTime.now(), dueDate, importanceService.getImportances()
                        .stream()
                        .filter(e-> e.getLVL() == importanceLevel)
                        .findAny()
                        .orElseThrow()));
    }

    @PutMapping
    @RequestMapping(value = "todolist/edit", produces = ("application/json"))
    public TodoList edit(@RequestParam String oldName,
                         @RequestParam String name,
                         @RequestParam int r, @RequestParam int b, @RequestParam int g) {
        return service.edit(oldName, name, new Color(r, g, b));
    }

    @DeleteMapping
    @RequestMapping(value = "todolist/deleteItem")
    public int deleteItem(@RequestParam String listName, @RequestParam String nameOfItem) {
        return service.deleteItem(listName, nameOfItem);
    }

    @DeleteMapping
    @RequestMapping(value = "todolist/delete")
    public int delete(@RequestParam String name) {
        return service.delete(name);
    }

}
